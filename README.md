# ⚠️ Archival Notice ⚠️ 

This project was a part of Neoteric Design's internal tooling and prototypes. 

These are no longer officially supported or maintained, and may contain bugs or be in any stage of (in)completeness.

This repository is provided as a courtesy to our former clients to support their projects going forward, as well in the interest of giving back what we have to the community. If you found yourself here, we hope you find it useful in some capacity ❤️ 


--------------------

# Mappyland
A superlight, standalone library to build Google maps with a set of markers.


## Installation
Add this line to your application's Gemfile:

```ruby
gem 'mappyland'
```

And then execute:
```bash
$ bundle
```

Or install it yourself as:
```bash
$ gem install mappyland
```

Require mappyland in your application's javascript. This gem also includes
libraries for [clustering](https://github.com/googlemaps/js-marker-clusterer)
and [spidering](https://github.com/jawj/OverlappingMarkerSpiderfier) your
markers. These features will automatically be enabled if their respective
libraries are detected.

```javascript
//= require mappyland
//= require marker_clusterer
//= require oms
```

## Usage

Add the Google Maps Javascript API after your application javascript. It's
recommended to load the script asynchonously. The rest of the page won't be held
up by loading the Maps JS.

Note you'll also need a Maps JS API key set. As shown here, it's using Rails'
Secrets/Credentials. Also, this is set up to run Mappyland's default
initializer function on callback. See [Initialization](#initialization)

```html
<script async defer src="https://maps.googleapis.com/maps/api/js?callback=initMappyland&key=<%= Rails.application.secrets[:google_browser_key] %>"></script>
```

Add a container to the page where you'd like the map. Mappyland uses data
attributes to set marker data and map options.

### Map container

Mappyland is configured per element via data attributes. The gem includes a
Rails helper to easily build the proper markup.

```ruby
mappyland markers, html_options: {}, gmap: {}, auto_fit_to_markers: nil,
                   default_icon: nil
```

`markers`

Required. Your records as json. See [Serializing Records](#serializing-records)

`html_options`

Accepts a `:tag` key (default `:div` that sets the element type
of the container. All other options are passed to Rails' `content_tag`

`gmap`

Directly passed to the
[Google Map initializer](https://developers.google.com/maps/documentation/javascript/3.exp/reference#MapOptions)

`auto_fit_to_markers`

Enable/disable fitBounds to markers.

`default_icon`

Provide a custom default icon, when one isn't provided in a marker's data.

Example:
```ruby
<%= mappyland records_as_json_array,
              html_options: { tag: :div, id: 'my-cool-map' },
              default_icon: asset_path('cool-marker.png'),
              auto_fit_to_markers: false,
              gmap: { minZoom: 3 } %>
```

becomes something like

```html
<div id='my-cool-map' data-mappyland data-markers="SERIALIZED MARKER DATA"
     data-default-icon="/images/cool-marker.png"
     data-auto-fit-to-markers="false" data-gmap="{\"minZoom\":3}">
</div>
```

### Initialization

Mappyland can be initialized a number of ways. Including automatically when
used with the callback parameter in Google Maps.

Make `initMappyland` the callback, and when the Google Maps API is finished
loading, it will attempt to create the maps. If the API finishes loading before
page, it will also add listeners for `DOMContentLoaded`. If Turbolinks is
detected, it will instead use `turbolinks:load`.

If more control is needed, there are more options:

Build one map:
```javascript
mapContainer = document.querySelector('#some-map')
mapContainer.mappyland = new Mappyland(mapContainer)
```

Build every map for a given selector:

```javascript
createMappyland('.map')
```

### Working with Mappyland Objects

| Attribute              | Description                                |
|------------------------|--------------------------------------------|
|`gmap`                  |The Google Map object                       |
|`markers`               |ID-keyed object storing the refs to markers |
|`currentInfoWindow`     |Currently open infowindow, if any           |
|`markerBounds`          |The boundaries of the loaded marker set     |

| Function                       | Description                                |
|--------------------------------|--------------------------------------------|
|`fitToMarkers()`                |fitBound the map to the markers boundaries  |
|`centerOnMarker(marker)`        |Centers the map on a given marker. Uses first marker if none given |
|`findMarker(id)`                |Finds a marker based on ID                  |
|`triggerMarkerClick(idOrMarker)`|Simulate a click on a marker given either an id or the marker itself.|


Note: The provided initializer functions attach a `mappyland` object to the
given container.

### Serializing Records

Mappyland expects a JSON array of objects with the following attributes

|Attribute      |Required?   |Description                           |
|---------------|------------|--------------------------------------|
|`id`           |Required    |Unique identifier to refer to marker  |
|`title`        |Recommended |A title for the marker                |
|`latitude`     |Required    |Latitude of marker                    |
|`longitude`    |Required    |Longitude of marker                   |
|`icon`         |Optional    |URL or attribute set for marker icon  |
|`info_window`  |Optional    |HTML content string or attribute set for constructing infoWindow |

ActiveModel::Serializers is easy and clean way to generate these.

## License
The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).
