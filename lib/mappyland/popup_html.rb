module Mappyland
  class PopupHtml
    attr_reader :object, :title, :url

    def initialize(object, title, url)
      @object, @title, @url = object, title, url
    end

    def output
      output = "<a href='#{url}'>"
      output << "<h3>#{title}</h3>"
      output << '</a>'
    end
  end
end
