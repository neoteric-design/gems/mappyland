module Mappyland
  module MappylandHelper
    def mappyland(marker_data, html_options: {}, gmap: {},
                  **mappyland_options)
      html_tag = html_options.delete(:tag) || :div

      html_options[:class] ||= "map-container"
      html_options[:data] ||= {}
      html_options[:data].merge!(mappyland_options)
      html_options[:data].merge!(markers: marker_data,
                                 gmap: gmap,
                                 mappyland: '')

      content_tag html_tag, '', html_options
    end
  end
end
