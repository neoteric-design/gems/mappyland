$:.push File.expand_path('lib', __dir__)

# Maintain your gem's version:
require 'mappyland/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = 'mappyland'
  s.version     = Mappyland::VERSION
  s.authors     = ['Madeline Cowie']
  s.email       = ['madeline@cowie.me']
  s.homepage    = 'http://neotericdesign.com'
  s.summary     = 'Generate a Google Map with markers based on JSON data'
  s.description = 'Generate a Google Map with markers based on JSON data'
  s.license     = 'MIT'

  s.files = Dir['{app,lib,vendor}/**/*', 'MIT-LICENSE', 'Rakefile', 'README.md']

  s.add_dependency 'rails', '>= 4'

  s.add_development_dependency 'sqlite3'
end
