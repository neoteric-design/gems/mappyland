0.4.3
=====

* Loosen rails version restrictions as we head into the sunset

0.3.0
=====

* BREAKING: Change marker data attribute `popup` in favor of `info_window` to allow greater customization of each marker's infoWindow. Will handle plain strings as content.

* BREAKING: Change option `defaultIconPath` to `defaultIcon` and marker data `icon_path` to `icon` to better relate that it can be a set of attributes or a URL. See `icon` attribute in [MarkerOptions reference](https://developers.google.com/maps/documentation/javascript/reference#MarkerOptions)


0.2.0
=====

* BREAKING: Store markers as an object instead of array, keyed to the marker data's `id` property
* BREAKING: Rename ref to google map object from `map` to `gmap`
* BREAKING: Rename option `map_options` to `gmap_options` for clarity
* BREAKING: Rename `bounds` to `markerBounds` for clarity
* BREAKING: Rename `current_infowindow` to `currentInfoWindow` for conventional naming
* Add option: `autofitToMarkers` to disable the automatic fitBounding to the markers, default: true
* Add functions: `findMarker`, `fitToMarkers`, `triggerMarkerClick`
